<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-success px-5">
        <div class="container-fluid">
            <a class="navbar-brand d-flex align-items-center">
                <img src='./assets/dragon-favicon.png' class='mx-1' />
                Dragón
            </a>
            <button type="button" class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse4">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse4">
                <section class="d-flex ms-auto navbar-nav">
                    <a 
                        target='_blank' 
                        rel='noreferrer' 
                        href="https://glpi.victum-re.online/" 
                        class="navbar-brand"
                    >Mesa de ayuda</a>
                </section>
            </div>
        </div>        
    </nav>
</header>