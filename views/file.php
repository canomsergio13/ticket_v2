<?php

    session_start(); // Iniciar la sesión

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // Validar y procesar los datos del formulario
        $mail = $_POST['mail'];

        if (isset($_FILES['imagen']) && $_FILES['imagen']['error'] === UPLOAD_ERR_OK) {
            // Obtener información del archivo
            $nombreArchivo = $_FILES['imagen']['name'];
            $rutaTemporal = $_FILES['imagen']['tmp_name'];
        
            // Ruta de destino
            $rutaDestino = '/var/www/glpi/files/PNG/c3/' . $nombreArchivo;
        
            // Mover el archivo de la ruta temporal a la ruta de destino
            if (move_uploaded_file($rutaTemporal, $rutaDestino)) {
                echo 'Archivo subido exitosamente.';
            } else {
                echo 'Error al subir el archivo.';
            }
        } else {
            echo 'No se ha enviado ningún archivo o hubo un error en la subida.';
        }


        // Guardar los datos en la sesión
        $_SESSION['mail'] = $mail;

    }

    // Mostrar el formulario y utilizar los valores de la sesión si están disponibles
    $mail = isset($_SESSION['mail']) ? $_SESSION['mail'] : '';


?>


<!DOCTYPE html>
<html lang="es-MX">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ticket Rápido</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/styles.css">
</head>
<body>
    <?php include 'components/NavBar.php'; ?>

    <section class="d-flex justify-content-center align-items-center">
        <div class="card m-1 mt-3 shadow" style="width: 30rem;">
            <div class="card-body">
                <h4 class="text-center" >Ticket Rápido</h4>
                <form class="" action="" method="POST">
                    <div class="mb-3">
                        <label class="form-label">¿Quión solicita?:</label>
                        <input type="email" class="form-control" name="mail" placeholder="correo@mail.com">
                    </div>

                    <section class='d-flex justify-content-center my-4' >
                        <label class="custum-file-upload hasfile" htmlFor="file">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="" viewBox="0 0 24 24"><g strokeWidth="0" id="SVGRepo_bgCarrier"></g><g strokeLinejoin="round" strokeLinecap="round" id="SVGRepo_tracerCarrier"></g><g id="SVGRepo_iconCarrier"> <path fill="#5E5F61" d="M10 1C9.73478 1 9.48043 1.10536 9.29289 1.29289L3.29289 7.29289C3.10536 7.48043 3 7.73478 3 8V20C3 21.6569 4.34315 23 6 23H7C7.55228 23 8 22.5523 8 22C8 21.4477 7.55228 21 7 21H6C5.44772 21 5 20.5523 5 20V9H10C10.5523 9 11 8.55228 11 8V3H18C18.5523 3 19 3.44772 19 4V9C19 9.55228 19.4477 10 20 10C20.5523 10 21 9.55228 21 9V4C21 2.34315 19.6569 1 18 1H10ZM9 7H6.41421L9 4.41421V7ZM14 15.5C14 14.1193 15.1193 13 16.5 13C17.8807 13 19 14.1193 19 15.5V16V17H20C21.1046 17 22 17.8954 22 19C22 20.1046 21.1046 21 20 21H13C11.8954 21 11 20.1046 11 19C11 17.8954 11.8954 17 13 17H14V16V15.5ZM16.5 11C14.142 11 12.2076 12.8136 12.0156 15.122C10.2825 15.5606 9 17.1305 9 19C9 21.2091 10.7909 23 13 23H20C22.2091 23 24 21.2091 24 19C24 17.1305 22.7175 15.5606 20.9844 15.122C20.7924 12.8136 18.858 11 16.5 11Z" clipRule="evenodd" fillRule="evenodd"></path> </g></svg>
                            </div>
                            <input type="file" enctype="multipart/form-data" class="custom-file-input mt-1" id="customFileLang" name="image" accept="image/*" lang="es">
                        </label>
                    </section>

                    <div class="row">
                        <div class="col" >
                            <button class="btn btn-success py-2 w-100" type="submit" id="create" name="create">Crear Ticket</button>
                        </div>
                        <div class="col" >
                            <a href="/ticket/" class="btn btn-danger py-2 w-100" id="back" name="back">Atras</a>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
    </section>

    <?php include 'components/Scripts.php'; ?>
</body>
</html>