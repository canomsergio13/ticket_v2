<?php

    session_start(); // Iniciar la sesión

    if (isset($_POST['next'])) {
        // Validar y procesar los datos del formulario
        $title = $_POST['title'];
        $description = $_POST['description'];
        $types = $_POST['types'];
        $urgency = $_POST['urgency'];
        $impact = $_POST['impact'];
        $prio = $_POST['prio'];
        $origin = $_POST['origin'];

        // Guardar los datos en la sesión
        $_SESSION['title'] = $title;
        $_SESSION['description'] = $description;
        $_SESSION['types'] = $types;
        $_SESSION['urgency'] = $urgency;
        $_SESSION['impact'] = $impact;
        $_SESSION['prio'] = $prio;
        $_SESSION['origin'] = $origin;
    }

    if(isset($_POST['create'])){
        if (isset($_FILES['image']) && $_FILES['image']['error'] === UPLOAD_ERR_OK) {
            // Obtener información del archivo
            $nombreArchivo = $_FILES['image']['name'];
            $rutaTemporal = $_FILES['image']['tmp_name'];
        
            // Ruta de destino
            $rutaDestino = '/var/www/glpi/files/PNG/c3/' . $nombreArchivo;
        
            // Mover el archivo de la ruta temporal a la ruta de destino
            if (move_uploaded_file($rutaTemporal, $rutaDestino)) {
                echo 'Archivo subido exitosamente.';
            } else {
                echo 'Error al subir el archivo.';
            }
        } 
    }


    // Mostrar el formulario y utilizar los valores de la sesión si están disponibles
    $title = isset($_SESSION['title']) ? $_SESSION['title'] : '';
    $description = isset($_SESSION['description']) ? $_SESSION['description'] : '';
    $types = isset($_SESSION['types']) ? $_SESSION['types'] : '';
    $urgency = isset($_SESSION['urgency']) ? $_SESSION['urgency'] : '3';
    $impact = isset($_SESSION['impact']) ? $_SESSION['impact'] : '3';
    $prio = isset($_SESSION['prio']) ? $_SESSION['prio'] : '3';
    $origin = isset($_SESSION['origin']) ? $_SESSION['origin'] : '';

?>

<!DOCTYPE html>
<html lang="es-MX">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ticket Rápido</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/styles.css">
</head>
<body>
    <?php include 'components/NavBar.php'; ?>


    <?php if(isset($_POST['next'])){ ?>

        <section class="d-flex justify-content-center align-items-center">
            <div class="card m-1 mt-3 shadow" style="width: 30rem;">
                <div class="card-body">
                    <h4 class="text-center" >Ticket Rápido</h4>
                    <form class="" action="" method="POST">
                        <div class="mb-3">
                            <label class="form-label">¿Quión solicita?:</label>
                            <input type="email" class="form-control" name="mail" placeholder="correo@mail.com">
                        </div>

                        <section class='d-flex justify-content-center my-4' >
                            <label class="custum-file-upload hasfile" htmlFor="file">
                                <div class="icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="" viewBox="0 0 24 24"><g strokeWidth="0" id="SVGRepo_bgCarrier"></g><g strokeLinejoin="round" strokeLinecap="round" id="SVGRepo_tracerCarrier"></g><g id="SVGRepo_iconCarrier"> <path fill="#5E5F61" d="M10 1C9.73478 1 9.48043 1.10536 9.29289 1.29289L3.29289 7.29289C3.10536 7.48043 3 7.73478 3 8V20C3 21.6569 4.34315 23 6 23H7C7.55228 23 8 22.5523 8 22C8 21.4477 7.55228 21 7 21H6C5.44772 21 5 20.5523 5 20V9H10C10.5523 9 11 8.55228 11 8V3H18C18.5523 3 19 3.44772 19 4V9C19 9.55228 19.4477 10 20 10C20.5523 10 21 9.55228 21 9V4C21 2.34315 19.6569 1 18 1H10ZM9 7H6.41421L9 4.41421V7ZM14 15.5C14 14.1193 15.1193 13 16.5 13C17.8807 13 19 14.1193 19 15.5V16V17H20C21.1046 17 22 17.8954 22 19C22 20.1046 21.1046 21 20 21H13C11.8954 21 11 20.1046 11 19C11 17.8954 11.8954 17 13 17H14V16V15.5ZM16.5 11C14.142 11 12.2076 12.8136 12.0156 15.122C10.2825 15.5606 9 17.1305 9 19C9 21.2091 10.7909 23 13 23H20C22.2091 23 24 21.2091 24 19C24 17.1305 22.7175 15.5606 20.9844 15.122C20.7924 12.8136 18.858 11 16.5 11Z" clipRule="evenodd" fillRule="evenodd"></path> </g></svg>
                                </div>
                                <input type="file" enctype="multipart/form-data" class="custom-file-input mt-1" id="customFileLang" name="image" accept="image/*" lang="es">
                            </label>
                        </section>

                        <div class="row">
                            <div class="col" >
                                <button class="btn btn-success py-2 w-100" type="submit" id="create" name="create">Crear Ticket</button>
                            </div>
                            <div class="col" >
                                <a href="/ticket/" class="btn btn-danger py-2 w-100" id="back" name="back">Atras</a>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </section>

    <?php } else { ?>
        <section class="d-flex justify-content-center align-items-center">
            <div class="card m-1 mt-3 shadow" style="width: 30rem;">
                <div class="card-body">
                    <h4 class="text-center" >Ticket Rápido</h4>
                    <form class="" action="" method="POST">
                        <div class="mb-2">
                            <label class="form-label">Titulo:</label>
                            <select class="form-select" name="title" id="title">
                                <option value="PROBLEMA CON AUDIO DEL EQUIPO DE CÓMPUTO" <?= $title === 'PROBLEMA CON AUDIO DEL EQUIPO DE CÓMPUTO' ? 'selected' : '' ?> >PROBLEMA CON AUDIO DEL EQUIPO DE CÓMPUTO</option>
                                <option value="ACTUALIZACIONES DEL SISTEMA OPERATIVO" <?= $title === 'ACTUALIZACIONES DEL SISTEMA OPERATIVO' ? 'selected' : '' ?> >ACTUALIZACIONES DEL SISTEMA OPERATIVO</option>
                                <option value="PANTALLA AZUL WINDOWS" <?= $title === 'PANTALLA AZUL WINDOWS' ? 'selected' : '' ?> >PANTALLA AZUL WINDOWS</option>
                                <option value="APLICACIÓN DE GOOGLE (HOJA DE CÁLCULO, CORREO, DOCUMENTO)" <?= $title === 'APLICACIÓN DE GOOGLE (HOJA DE CÁLCULO, CORREO, DOCUMENTO)' ? 'selected' : '' ?> >APLICACIÓN DE GOOGLE (HOJA DE CÁLCULO, CORREO, DOCUMENTO)</option>
                                <option value="RECUPERAR UN CORREO" <?= $title === 'RECUPERAR UN CORREO' ? 'selected' : '' ?> >RECUPERAR UN CORREO</option>
                                <option value="ACCESO A CARPETAS COMPARTIDAS" <?= $title === 'ACCESO A CARPETAS COMPARTIDAS' ? 'selected' : '' ?> >ACCESO A CARPETAS COMPARTIDAS</option>
                                <option value="IMPRESORAS" <?= $title === 'IMPRESORAS' ? 'selected' : '' ?> >IMPRESORAS</option>
                                <option value="PREPARACIÓN DE NUEVO EQUIPO" <?= $title === 'PREPARACIÓN DE NUEVO EQUIPO' ? 'selected' : '' ?> >PREPARACIÓN DE NUEVO EQUIPO</option>
                                <option value="VIDEOCONFERENCIA EN SALA DE JUNTAS" <?= $title === '' ? 'selected' : '' ?> >VIDEOCONFERENCIA EN SALA DE JUNTAS</option>
                                <option value="SERVICIO ESPECIAL PARA PERSONAL V.I.P." <?= $title === 'SERVICIO ESPECIAL PARA PERSONAL V.I.P.' ? 'selected' : '' ?> >SERVICIO ESPECIAL PARA PERSONAL V.I.P.</option>
                                <option value="CHECKLIST INFRAESTRUCTURA Y APLICACIONES DRAGON" <?= $title === '' ? 'selected' : '' ?> >CHECKLIST INFRAESTRUCTURA Y APLICACIONES DRAGON</option>
                                <option value="PROBLEMAS CON LA CONEXIÓN POR VPN" <?= $title === 'PROBLEMAS CON LA CONEXIÓN POR VPN' ? 'selected' : '' ?> >PROBLEMAS CON LA CONEXIÓN POR VPN</option>
                                <option value="PROBLEMAS CON LAS PÁGINAS O PORTALES DE EMPRESAS DRAGON" <?= $title === '' ? 'selected' : '' ?> >PROBLEMAS CON LAS PÁGINAS O PORTALES DE EMPRESAS DRAGON</option>
                                <option value="ERROR EN UN PROCESO JDE- COMPRAS" <?= $title === 'ERROR EN UN PROCESO JDE- COMPRAS' ? 'selected' : '' ?> >ERROR EN UN PROCESO JDE- COMPRAS</option>
                                <option value="ERROR EN UN PROCESO JDE-FACTURACIÓN" <?= $title === 'ERROR EN UN PROCESO JDE-FACTURACIÓN' ? 'selected' : '' ?> >ERROR EN UN PROCESO JDE-FACTURACIÓN</option>
                                <option value="ERROR EN UN PROCESO JDE- ALMACEN" <?= $title === 'ERROR EN UN PROCESO JDE- ALMACEN' ? 'selected' : '' ?> >ERROR EN UN PROCESO JDE- ALMACEN</option>
                                <option value="ERROR EN UN PROCESO JDE-VENTAS" <?= $title === 'ERROR EN UN PROCESO JDE-VENTAS' ? 'selected' : '' ?> >ERROR EN UN PROCESO JDE-VENTAS</option>
                                <option value="ERROR EN UN PROCESO JDE-FISCAL" <?= $title === 'ERROR EN UN PROCESO JDE-FISCAL' ? 'selected' : '' ?> >ERROR EN UN PROCESO JDE-FISCAL</option>
                                <option value="ERROR EN UN PROCESO JDE-CONTABILIDAD Y FINANZAS" <?= $title === 'ERROR EN UN PROCESO JDE-CONTABILIDAD Y FINANZAS' ? 'selected' : '' ?> >ERROR EN UN PROCESO JDE-CONTABILIDAD Y FINANZAS</option>
                                <option value="PROBLEMA CON OSMOS" <?= $title === 'PROBLEMA CON OSMOS' ? 'selected' : '' ?> >PROBLEMA CON OSMOS</option>
                                <option value="PROBLEMA CON QLINK VIEW" <?= $title === 'PROBLEMA CON QLINK VIEW' ? 'selected' : '' ?> >PROBLEMA CON QLINK VIEW</option>
                                <option value="PROBLEMA CON PSICUM" <?= $title === 'PROBLEMA CON PSICUM' ? 'selected' : '' ?> >PROBLEMA CON PSICUM</option>
                                <option value="PROBLEMA CON ENDTOEND" <?= $title === 'PROBLEMA CON ENDTOEND' ? 'selected' : '' ?> >PROBLEMA CON ENDTOEND</option>
                                <option value="PROBLEMA CON RELOJ CHECADOR" <?= $title === 'PROBLEMA CON RELOJ CHECADOR' ? 'selected' : '' ?> >PROBLEMA CON RELOJ CHECADOR</option>
                                <option value="TELÉFONO - CELULAR O EXTENSIÓN" <?= $title === 'TELÉFONO - CELULAR O EXTENSIÓN' ? 'selected' : '' ?> >TELÉFONO - CELULAR O EXTENSIÓN</option>
                                <option value="RESPALDO DE INFORMACIÓN" <?= $title === 'RESPALDO DE INFORMACIÓN' ? 'selected' : '' ?> >RESPALDO DE INFORMACIÓN</option>
                                <option value="SOLICITUD DE NUEVO SERVICIO" <?= $title === 'SOLICITUD DE NUEVO SERVICIO' ? 'selected' : '' ?> >SOLICITUD DE NUEVO SERVICIO</option>
                                <option value="REPORTES DE GASTO" <?= $title === 'REPORTES DE GASTO' ? 'selected' : '' ?> >REPORTES DE GASTO</option>
                            </select>
                        </div>
    
                        <div class="mb-2">
                            <label class="form-label">Descripción:</label>
                            <textarea class="form-control" rows="3" cols="5" name="description" id="description"><?= $description ?></textarea>
                        </div>
    
                        <div class="mb-2" >
                            <label  class="form-label">Tipo:</label>
                            <select class="form-select" name="types" id="types">
                                <option value="1" <?= $types === '1' ? 'selected' : '' ?> >Inicidente</option>
                                <option value="2" <?= $types === '2' ? 'selected' : '' ?> >Solicitud</option>
                            </select>
                        </div>
    
                        <div class="mb-2 row">
                            <div class="col" >
                                <label  class="form-label">Urgencia:</label>
                                <select class="form-select" name="urgency" id="urgency">
                                    <option value="1" <?= $urgency === '1' ? 'selected' : '' ?> >Muy baja</option>
                                    <option value="2" <?= $urgency === '2' ? 'selected' : '' ?> >Baja</option>
                                    <option value="3" <?= $urgency === '3' ? 'selected' : '' ?> >Media</option>
                                    <option value="4" <?= $urgency === '4' ? 'selected' : '' ?> >Alta</option>
                                    <option value="5" <?= $urgency === '5' ? 'selected' : '' ?> >Muy alta</option>
                                </select>
                            </div>
                            <div class="col" >
                                <label  class="form-label">Impacto:</label>
                                <select class="form-select" name="impact" id="impact">
                                    <option value="1" <?= $impact === '1' ? 'selected' : '' ?> >Muy baja</option>
                                    <option value="2" <?= $impact === '2' ? 'selected' : '' ?> >Baja</option>
                                    <option value="3" <?= $impact === '3' ? 'selected' : '' ?> >Media</option>
                                    <option value="4" <?= $impact === '4' ? 'selected' : '' ?> >Alta</option>
                                    <option value="5" <?= $impact === '5' ? 'selected' : '' ?> >Muy alta</option>
                                </select>
                            </div>
                        </div>
    
                        <div class="mb-3 row">
                            <div class="col" >
                                <label class="form-label">Prioridad:</label>
                                <select class="form-select" name="prio" id="prio">
                                    <option value="1" <?= $prio === '1' ? 'selected' : '' ?> >Muy baja</option>
                                    <option value="2" <?= $prio === '2' ? 'selected' : '' ?> >Baja</option>
                                    <option value="3" <?= $prio === '1' ? 'selected' : '' ?> >Media</option>
                                    <option value="4" <?= $prio === '4' ? 'selected' : '' ?> >Alta</option>
                                    <option value="5" <?= $prio === '5' ? 'selected' : '' ?> >Muy alta</option>
                                    <option value="6" <?= $prio === '6' ? 'selected' : '' ?> >Mayor</option>
                                </select>
                            </div>
                            <div class="col" >
                                <label class="form-label">Origen de solicitud:</label>
                                <select class="form-select" name="origin" id="origin">
                                    <option value="1" <?= $origin === '1' ? 'selected' : '' ?> >Helpdesk</option>
                                    <option value="2" <?= $origin === '2' ? 'selected' : '' ?> >E-Mail</option>
                                    <option value="3" <?= $origin === '3' ? 'selected' : '' ?> >Phone</option>
                                    <option value="4" <?= $origin === '4' ? 'selected' : '' ?> >Direct</option>
                                    <option value="5" <?= $origin === '5' ? 'selected' : '' ?> >Written</option>
                                </select>
                            </div>
                        </div>
    
                        <div class="">
                            <button class="btn btn-success py-2 w-100" type="submit" id="next" name="next">Siguiente</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    <?php } ?>



    <?php include 'components/Scripts.php'; ?>
</body>
</html>